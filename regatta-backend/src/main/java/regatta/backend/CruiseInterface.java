package regatta.backend;

import java.util.List;

import javax.ejb.Local;

import regatta.backend.model.Boat;
import regatta.backend.model.type.BoatType;

@Local
public interface CruiseInterface {
	
	public void addYacht(String yachtName, Integer seats, String beds, BoatType type);
	
	public void signUp(String yachtName);
	
	public void informCapitan(String yachtName, String email);

	public List<Boat> getYachtsWithFreeSeats();
}
