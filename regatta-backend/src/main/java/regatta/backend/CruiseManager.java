package regatta.backend;

import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import regatta.backend.db.DataBase;
import regatta.backend.dto.Repository;
import regatta.backend.model.Boat;
import regatta.backend.model.type.BoatType;
import javax.ejb.Lock;
import javax.ejb.LockType;

@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class CruiseManager implements CruiseInterface{
	
	@Inject
    @DataBase
    private Repository repo;
		
	@Lock(LockType.READ)
	@Override
	public void addYacht(String yachtName, Integer seats, String beds, BoatType type) {
	
		Boat boat = new Boat(yachtName, seats, beds , type);
		repo.insertBoat(boat);
	}

	@Lock(LockType.READ)
	@Override
	public void signUp(String yachtName) {
		repo.singUpToBoat(yachtName);
	}
	
	@Override
	public void informCapitan(String yachtName, String email) {
		System.out.println("Użytkownik " + email + " chce się zapisać na jacht " + yachtName);
	}
	
	@Override
	public List<Boat> getYachtsWithFreeSeats(){
		return repo.findFreeBoats();
	}
}
