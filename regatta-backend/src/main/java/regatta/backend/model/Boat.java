package regatta.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import regatta.backend.model.type.BoatType;

@SuppressWarnings("serial")
@Entity
@Table(
    name = "boats"
)

public class Boat extends Model{
	
    public Boat(String boatName, Integer maxCrew, String bedsNumber, BoatType boatType) {
    	this.boatName = boatName;
    	this.maxCrew = maxCrew.toString();
    	this.bedsNumber = bedsNumber;
    	this.boatType = boatType;
    	this.freeSeats = maxCrew;
    }
    public String getBoatName() {
		return boatName;
	}
	public void setBoatName(String boatName) {
		this.boatName = boatName;
	}
	public String getMaxCrew() {
		return maxCrew;
	}
	public void setMaxCrew(String maxCrew) {
		this.maxCrew = maxCrew;
	}
	public String getBedsNumber() {
		return bedsNumber;
	}
	public void setBedsNumber(String bedsNumber) {
		this.bedsNumber = bedsNumber;
	}
	public BoatType getBoatType() {
		return boatType;
	}
	public void setBoatType(BoatType boatType) {
		this.boatType = boatType;
	}
	public Integer getFreeSeats() {
		return freeSeats;
	}
	public void setFreeSeats(Integer freeSeats) {
		this.freeSeats = freeSeats;
	}
	public List<CrewMember> getCrew() {
		return crew;
	}
	public void setCrew(List<CrewMember> crew) {
		this.crew = crew;
	}
	public Boat() {}
	
	@Column(name = "boat_name", columnDefinition = "varchar(20) NOT NULL COMMENT 'Nazwa lodki'")
    private String boatName;

	@Column(name = "max_crew", columnDefinition = "varchar(2) NOT NULL COMMENT 'Maksymalna ilosc osob na pokladzie'")
    private String maxCrew;
	
    @Column(name = "beds_number", columnDefinition = "varchar(2) NOT NULL COMMENT 'Ilosc koi'")
    private String bedsNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "enum('OPAL', 'VEGA', 'OPTYMIST') NOT NULL COMMENT 'Typ lodzi'")
    private BoatType boatType;
    
	@Column(name = "free_seats", columnDefinition = "int(2) NOT NULL COMMENT 'Wolne miejsca'")
    private Integer freeSeats;
	
    @OneToMany(fetch = FetchType.LAZY, mappedBy="boat")
    private List<CrewMember> crew = new ArrayList<>();
   
}
