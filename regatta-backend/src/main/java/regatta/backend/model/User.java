package regatta.backend.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import regatta.backend.model.Model;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class User extends Model {

    @Column(columnDefinition = "varchar(20) NOT NULL COMMENT 'Imię osoby'")
    protected String name;

    @Column(columnDefinition = "varchar(20) NOT NULL COMMENT 'Nazwisko osoby'")
    protected String surname;

    @Column(columnDefinition = "varchar(80) COMMENT 'Adres email osoby'")
    protected String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
