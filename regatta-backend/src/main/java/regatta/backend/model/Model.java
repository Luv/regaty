package regatta.backend.model;

import java.io.Serializable;
import javax.persistence.*;

import regatta.backend.db.QuerisNames;

@NamedQueries({
	@NamedQuery(
			name = QuerisNames.BOATS_BY_FREE_SEATS,
			query = "SELECT b FROM Boat b WHERE b.freeSeats > 0"
	), 
	@NamedQuery(
			name = QuerisNames.DECREASE_FREE_SEATS,
			query = "UPDATE Boat b SET b.freeSeats = b.freeSeats - 1 WHERE b.boatName = :name"
	) 
})

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11) UNSIGNED COMMENT 'Identyfikator tabeli'")
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
