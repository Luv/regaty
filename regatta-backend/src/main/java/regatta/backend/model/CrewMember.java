package regatta.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.*;
import javax.persistence.Table;

import regatta.backend.model.type.SailingDegree;

@SuppressWarnings("serial")

@Entity
@Table(name = "crew_members")
public class CrewMember extends User {
	
	public CrewMember() {}
	
	public CrewMember(String name, String surname, String email, SailingDegree degree, Boat boat) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.sailingDegree = degree;
		this.boat = boat;
	}
	
    @Enumerated(EnumType.STRING)
    @Column(name = "sailing_degree", columnDefinition = "enum('CAPTAIN', 'SKIPPER', 'SAILOR') COMMENT 'Stopien zeglarski'")
    private SailingDegree sailingDegree;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "boat_id", 
        referencedColumnName = "id", 
        columnDefinition = "int(11) unsigned COMMENT 'Identyfikator lodki'"
    )
    private Boat boat;

	public SailingDegree getSailingDegree() {
		return sailingDegree;
	}

	public void setSailingDegree(SailingDegree sailingDegree) {
		this.sailingDegree = sailingDegree;
	}

	public Boat getBoat() {
		return boat;
	}

	public void setBoat(Boat boat) {
		this.boat = boat;
	}
    
}

