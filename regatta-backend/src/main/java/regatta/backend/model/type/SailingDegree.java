package regatta.backend.model.type;

public enum SailingDegree {

    CAPTAIN, SKIPPER, SAILOR
}