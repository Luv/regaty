package regatta.backend.model.type;

public enum BoatType {

	    OPAL, VEGA, OPTYMIST
}
