package regatta.backend.dto;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"yachtName", "maxCrew"})
@XmlAccessorType(FIELD)
public class YachtInfo {

    @XmlElement(name = "yn", required = true)
    private String yachtName;

    @XmlElement(name = "mc", required = true)
    private Integer maxCrew;

    @XmlTransient
    private Integer bunkNumber;

    public YachtInfo() { }

    public YachtInfo(String yachtName, Integer maxCrew, Integer bunkNumber) {
        this.yachtName = yachtName;
        this.maxCrew = maxCrew;
        this.bunkNumber = bunkNumber;
    }

	public String getYachtName() {
		return yachtName;
	}

	public void setYachtName(String yachtName) {
		this.yachtName = yachtName;
	}

	public Integer getMaxCrew() {
		return maxCrew;
	}

	public void setMaxCrew(Integer maxCrew) {
		this.maxCrew = maxCrew;
	}

	public Integer getBunkNumber() {
		return bunkNumber;
	}

	public void setBunkNumber(Integer bunkNumber) {
		this.bunkNumber = bunkNumber;
	}

}
