package regatta.backend.dto;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;

import regatta.backend.model.Boat;

@SessionScoped
@Dependent
public interface Repository extends Serializable {
	
	public Boat insertBoat(Boat boat);
	
	public List<Boat> findFreeBoats();
	
	public int singUpToBoat(String yachtName);
}
