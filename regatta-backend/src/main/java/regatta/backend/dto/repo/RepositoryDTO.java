package regatta.backend.dto.repo;

import java.util.List;
import javax.inject.Inject;
import regatta.backend.db.QuerisNames;
import regatta.backend.dto.Repository;
import regatta.backend.model.Boat;
import regatta.backend.model.CrewMember;
import regatta.backend.model.type.SailingDegree;
import regatta.backend.db.ManagerDB;
import regatta.backend.db.DataBase;

@SuppressWarnings("serial")
@DataBase
public class RepositoryDTO implements Repository{
	
	@Inject
    private ManagerDB db;
		
	@Override
	public Boat insertBoat(Boat boat) {	
		return	db.persist(boat);
	}
	
	@Override
	public List<Boat> findFreeBoats(){	
		return db.executeNamed(QuerisNames.BOATS_BY_FREE_SEATS, Boat.class);
	}
	
	@Override
	public int singUpToBoat(String yachtName) {
		Boat b = db.findByField("boatName", yachtName, Boat.class);
		CrewMember crewMember = new CrewMember("Jan", "Kowalski", "raz@wp.pl", SailingDegree.SKIPPER, b);
		db.persist(crewMember);
		return db.executeNamed(
				QuerisNames.DECREASE_FREE_SEATS,
				ManagerDB.createQueryParameter("name", yachtName)
			);
	}
}
