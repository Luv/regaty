package servlet;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashSet;
import java.util.Set;


@WebFilter(filterName = "/EmailFilter", urlPatterns = { "/home" })
public class EmailFilter implements Filter {
	
	private Set<String> emails = null;
	
    public void destroy() {
        System.out.println("Destroy EmailFilter");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       
    	if(request instanceof HttpServletRequest) {
            HttpServletRequest req = (HttpServletRequest)request;
          
            if(req.getParameterMap().containsKey("email") && !req.getParameter("email").isEmpty()) {
            	String email = req.getParameter("email");
            	if (emails.contains(email)) req.getSession().setAttribute("approved", "true");
            	else req.getSession().setAttribute("approved", "false");
        		req.getSession().setAttribute("email", email);
        	
        		System.out.println(email);
        		if(email.equals("admin@wp.pl")) ((HttpServletResponse)response).sendRedirect(req.getContextPath() + "/admin");
        		else ((HttpServletResponse)response).sendRedirect(req.getContextPath() + "/cruise");
            }
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig fConfig) throws ServletException {
        System.out.println("Init EmailFilter");
        emails = new HashSet<String>();
    	emails.add("raz@wp.pl");
    	emails.add("dwa@wp.pl");   	
    }

}
