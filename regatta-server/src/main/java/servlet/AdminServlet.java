package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import regatta.backend.model.type.BoatType;

@SuppressWarnings("serial")
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	

    public AdminServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	response.setContentType("text/html; charset=utf-8");
    	
    	response.getWriter().append("<html><body>");
        response.getWriter().append("<h2>Panel administracyjny</h2>");
        
        response.getWriter().append("<h3>Dodaj jacht:</h3>");
        response.getWriter().append("<form name=\"addYacht\" method=\"post\" action=\"" + request.getContextPath() + "/rest/cruise/addYacht\">");
        response.getWriter().append("<label for=\"yachtName\">Nazwa jachtu:</label>");
        response.getWriter().append("<input type=\"text\" name=\"yachtName\" />");
        response.getWriter().append("<label for=\"maxCrew\"> Maksymalna za�oga:</label>");
        response.getWriter().append("<input type=\"text\" name=\"maxCrew\" />");
        response.getWriter().append("<label for=\"bedsNumber\"> Liczba koi:</label>");
        response.getWriter().append("<input type=\"text\" name=\"bedsNumber\" />");        
        response.getWriter().append("<label for=\"boatType\">Typ łodzi:</label>");
        response.getWriter().append("<select class=\"form-control\" name=\"boatType\">");
        	for (BoatType type : BoatType.values()) response.getWriter().append("<option>" + type + "</option>");
        response.getWriter().append("<input type=\"submit\" />");
        response.getWriter().append("</form>");   
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
