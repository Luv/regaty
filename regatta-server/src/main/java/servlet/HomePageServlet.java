package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/home")
public class HomePageServlet extends HttpServlet {

	public HomePageServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
    	response.setContentType("text/html; charset=utf-8");
    	
    	response.getWriter().append("<html>");
        response.getWriter().append("<body>");
        response.getWriter().append("<h1>");
        response.getWriter().append("Klub zeglarski");
        response.getWriter().append("</h1>");
        response.getWriter().append("<form method=\"post\">");
        response.getWriter().append("<label for=\"email\">email:</label>");
        response.getWriter().append("<input type=\"text\" name=\"email\" />");
        response.getWriter().append("<input type=\"submit\" />");
        response.getWriter().append("</form>");
        response.getWriter().append("</body>");
        response.getWriter().append("</html>");
        
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    	
    	System.out.println("Post method");
    }

}
