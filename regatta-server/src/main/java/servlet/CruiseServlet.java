package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import regatta.backend.model.Boat;
import regatta.backend.CruiseInterface;

@SuppressWarnings("serial")
@WebServlet("/cruise")
public class CruiseServlet extends HttpServlet {
	
	@EJB
	CruiseInterface cm;

    public CruiseServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
	
		response.setContentType("text/html; charset=utf-8");
		PrintWriter rw = response.getWriter();
    	
		rw.append("<html>");
		if(request.getSession(true).getAttribute("approved").equals("true")) {
        	rw.append("<p>Wybierz na jaki jacht chcesz sie zapisac:</p>");
        	rw.append("<form name=\"yachtForm\" method=\"post\" action=\"" + request.getContextPath() + "/rest/cruise/signUp\">"); 
		} else {
			rw.append("<p>Wybierz na jaki jacht chcesz sie zapisac i zaczekaj na zgodę od kapitana:</p>");
			rw.append("<form name=\"yachtForm\" method=\"post\" action=\"" + request.getContextPath() + "/rest/cruise/askCaptain\">");
			rw.append("<input type=\"hidden\" name=\"email\" value=\"" + request.getSession().getAttribute("email") + "\" />");
		}
        rw.append("<select class=\"form-control\" name=\"select_yacht\">");  
        for (Boat yacht : cm.getYachtsWithFreeSeats()) {
        		rw.append("<option>" + yacht.getBoatName() + "</option>");
        }   
        rw.append("<input type=\"submit\"/>");
        rw.append("</form>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}