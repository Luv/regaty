package rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import regatta.backend.CruiseInterface;
import regatta.backend.model.type.BoatType;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;


@RequestScoped
@Path("/cruise")
public class CruiseServices{
	
	@EJB
	CruiseInterface cm;
	
    @Context
    private HttpServletRequest request;
    
    @PostConstruct
    private void init() {
        System.out.println("ejb: " + cm);
    }

    @POST
    @Path("/signUp")
    public Response signUp(@FormParam("select_yacht") String yachtName) {
    	cm.signUp(yachtName);
        return Response.ok("{\"status\": \"ok\",\"jacht\":\"" + yachtName + "\"}").build();
    }
    @POST
    @Path("/askCaptain")
    public Response askCaptain(@FormParam("select_yacht") String yachtName, @FormParam("email") String email) {
    	cm.informCapitan(yachtName, email);
        return Response.ok("{\"status\": \"ok\",\"jacht\":\"" + yachtName + " " + email + "\"}").build();
    }    
    
    @POST
    @Path("/addYacht")
    public Response addYacht(@FormParam("yachtName") String name, @FormParam("maxCrew") Integer maxCrew, @FormParam("bedsNumber") String bedsNumber, @FormParam("boatType") BoatType boatType) {
    	cm.addYacht(name, maxCrew, bedsNumber, boatType);
        return Response.ok("{\"status\": \"ok\",\"jacht\":\"" + name + " " + maxCrew + " " + bedsNumber + " " + boatType + "\"}").build();
    }     
}
